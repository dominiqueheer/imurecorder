package com.uantwerpen.dheer.hrtfrecorder;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    Vector3 measGyro, measAccel, measCompass, measCompassUncalib;

    FileOutputStream fos = null;

    long startTime;

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;

    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format

    int PERMISSION_REQUEST_ID = 123;

    int totalAudioSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // init sensor
        SensorManager mSensorManager = (SensorManager) getSystemService(getBaseContext().SENSOR_SERVICE);
        Sensor gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor compass = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor compassUncalib = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);

        mSensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, compass, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, compassUncalib, SensorManager.SENSOR_DELAY_FASTEST);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_REQUEST_ID);
        }

        final Button startButton = findViewById(R.id.button_id_start);
        final Button stopButton = findViewById(R.id.button_id_stop);
        final Button calibButton = findViewById(R.id.button_id_calibrate);
        final TextView textStatus = findViewById(R.id.textStatus);
        final TextView textTime = findViewById(R.id.textTime);
        final CheckBox cboxAutostop = findViewById(R.id.checkbox_id_autostop);
        final CheckBox cboxAutostart = findViewById(R.id.checkbox_id_autostart);
        final TextView textSecondsAutostart = findViewById(R.id.textbox_id_seconds_autostart);
        final TextView textSecondsAutostop = findViewById(R.id.textbox_id_seconds_autostop);
        final Switch switchHelp = findViewById(R.id.switch_id_help);
        final TextView textStoragePath = findViewById(R.id.text_help2);
        textStoragePath.setText(("Storage path: "  + getBaseContext().getExternalFilesDir(null).toString()));

        // auto start checkbox clicked
        cboxAutostart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textSecondsAutostart.setVisibility(cboxAutostart.isChecked() ? View.VISIBLE : View.GONE);
                textSecondsAutostart.setEnabled(cboxAutostart.isChecked());
            }
        });

        // auto stop checkbox clicked
        cboxAutostop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textSecondsAutostop.setVisibility(cboxAutostop.isChecked() ? View.VISIBLE : View.GONE);
                textSecondsAutostop.setEnabled(cboxAutostop.isChecked());
            }
        });

        // toggle help
        switchHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.text_help0).setVisibility(switchHelp.isChecked() ? View.VISIBLE : View.GONE);
                findViewById(R.id.text_help1).setVisibility(switchHelp.isChecked() ? View.VISIBLE : View.GONE);
                findViewById(R.id.text_help2).setVisibility(switchHelp.isChecked() ? View.VISIBLE : View.GONE);
            }
        });

        // start button clicked
        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (cboxAutostart.isChecked()) {
                    textStatus.setText("Status: Waiting for autostart");
                    int seconds = Integer.parseInt(textSecondsAutostart.getText().toString());
                    try {
                        Thread.sleep((seconds-1) * 1000);
                        vibrate(250);
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                File path = getBaseContext().getExternalFilesDir(null);
                File file = new File(path, "IMU_Data.txt");
                if (file.exists())
                    file.delete();

                try {
                    fos = new FileOutputStream(file);
                    startTime = System.currentTimeMillis();
                    isRecording = true;

                    stopButton.setClickable(true);
                    startButton.setClickable(false);

                    totalAudioSize = 0;

                    // start audio recording
                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                            44100, AudioFormat.CHANNEL_IN_STEREO,
                            AudioFormat.ENCODING_PCM_16BIT, BufferElements2Rec * BytesPerElement);

                    recorder.startRecording();
                    recordingThread = new Thread(new Runnable() {
                        public void run() {
                            writeAudioDataToFile();
                        }
                    }, "AudioRecorder Thread");
                    recordingThread.start();

                    textStatus.setText("Status: Recording");
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // stop button clicked
        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopRecording();
            }
        });

        // calibration button clicked
        calibButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File path = getBaseContext().getExternalFilesDir(null);
                File file = new File(path, "calibration.txt");

                if (file.exists())
                    file.delete();

                try {
                    fos = new FileOutputStream(file);
                    startTime = System.currentTimeMillis();
                    isRecording = true;

                    textStatus.setText("Status: Calibrating");
                    showMessage("Move phone for 60 seconds");
                    calibButton.setClickable(false);

                    // stop calibration automatically after 1 minute
                    final Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                synchronized (this) {
                                    wait(60000);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            isRecording = false;
                                            textStatus.setText("Status: Idle");
                                            try {
                                                fos.close();
                                            }
                                            catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            vibrate(500);
                                            calibButton.setClickable(true);
                                            showMessage("Calibration finished");
                                        }
                                    });
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // update time and memory usage every second
        final TextView textMemory = findViewById(R.id.textMemory);
        final Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        while (!this.isInterrupted()) {
                            wait(1000);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isRecording)
                                        return;

                                    textMemory.setText(("Audio data: " + Integer.toString(totalAudioSize / 1000 / 1000) + "MB"));
                                    long now = System.currentTimeMillis();
                                    long timestamp = (now - startTime) / 1000;
                                    textTime.setText(("Time: " + Long.toString(timestamp) + "s"));

                                    // check if we have to auto stop recording
                                    if (cboxAutostop.isChecked()) {
                                        int stopAfterSeconds = Integer.parseInt(textSecondsAutostop.getText().toString());
                                        if (stopAfterSeconds > 0 && timestamp >= stopAfterSeconds) {
                                            stopRecording();
                                            vibrate(500);
                                        }
                                    }
                                }
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    // vibrate for millis milliseconds
    private void vibrate(int millis) {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        } else{
            // deprecated in API 26
            v.vibrate(millis);
        }
    }

    private void showMessage(String message) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }

    /*private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }*/

    private void writeAudioDataToFile() {
        // Write the output audio in byte

        String filePath = getBaseContext().getExternalFilesDir(null).getPath() + "/recording.pcm";
        File f = new File(filePath);
        if (f.exists())
            f.delete();

        int minBufferSize = AudioRecord.getMinBufferSize(
                44100, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT);

        byte bData[] = new byte[minBufferSize * 2];

        //List<byte[]> dataList = new ArrayList<>();

        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (isRecording && recorder != null) {
            try {
                int readSize = recorder.read(bData, 0, minBufferSize);
                totalAudioSize += readSize;
                if (isRecording)
                    os.write(bData, 0, readSize);
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
        }
        try {
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        /*
        try {
            for (byte[] b : dataList) {
                os.write(b, 0, b.length);
            }
            os.close();
            dataList.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    private void stopRecording() {
        // stops the recording activity
        isRecording = false;
        if (null != recorder) {
            recorder.stop();
            recorder.release();
            recorder = null;
            recordingThread = null;
        }

        try {
            fos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        final Button startButton = findViewById(R.id.button_id_start);
        final Button stopButton = findViewById(R.id.button_id_stop);
        final TextView textStatus = findViewById(R.id.textStatus);

        stopButton.setClickable(false);
        startButton.setClickable(true);

        textStatus.setText("Status: Idle");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if  (!isRecording)
            return;

        long now = System.currentTimeMillis();
        float timestamp = (now - startTime) / 1000.0f;

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            measAccel = new Vector3(x, y, z);
        }

        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            measGyro = new Vector3(x, y, z);
        }

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
            measCompassUncalib = new Vector3(x, y, z);
        }

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            measCompass = new Vector3(x, y, z);
        }

        if (fos != null && measCompass != null && measCompassUncalib != null && measGyro != null && measAccel != null) {
            try {
                if (measAccel.valid && measGyro.valid && measCompass.valid && measCompassUncalib.valid) {
                    String s = "A " + measAccel.toString() + "\nG " + measGyro.toString() + "\nM " + measCompass.toString() + "\nU " + measCompassUncalib.toString() + "\n";
                    fos.write((Float.toString(timestamp) + "\n").getBytes());
                    fos.write(s.getBytes());
                    measAccel.invalidate();
                    measCompass.invalidate();
                    measGyro.invalidate();
                    measCompassUncalib.invalidate();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
