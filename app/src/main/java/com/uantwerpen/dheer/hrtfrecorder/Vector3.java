package com.uantwerpen.dheer.hrtfrecorder;

public class Vector3 {

    public float x, y, z;
    public boolean valid = false;

    public Vector3(float _x, float _y, float _z) {
        this.x = _x;
        this.y = _y;
        this.z = _z;
        this.valid = true;
    }

    public void invalidate() {
        this.valid = false;
    }

    @Override
    public String toString() {
        return Float.toString(this.x) + " " + Float.toString(this.y) + " " + Float.toString(this.z);
    }
}
